# ActyvAdmon (front-end)

This is the back-end side for ActyvAdmon, a activities and tasks administrator, where you can create activities or a "to do" list, even share it with another users.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 

### Prerequisites

You must have installed on your local machine:

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [ActyvAdmon (front-end)](https://gitlab.com/j_m_b/front-actyvadmon/)

### Installing

To run this project completely you must install ActyvAdmon (front-end), after that follow this steps to set up and run this side of the project. An advantage of docker is that with few commands you can build, configure and set up a project.

Download a copy of the project:

```
git clone https://gitlab.com/j_m_b/back-actyvadmon.git
```

Go into the project directory

```
cd back-actyvadmon
```

Initialize the project (this can take a while)

```
sudo docker-compose up --build
```

And thats all, you can access to this project by going to your [localhost:3000](http://localhost:3000/).

## Author

* **Jonathan Essau MB** - *Computer engineer* - [essau.co](https://essau.co/)

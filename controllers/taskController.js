const Task = require('../models/Task');
const Activity = require('../models/Activity');
const { validationResult } = require('express-validator');

// funcion que verifica errores definidos en las rutas
function issues(req, res){
    const errors = validationResult(req);
    if(!errors.isEmpty(0)){
        return res.status(400).json({errors: errors.array()});
    }
}

// funcion para verificar existencia y membresia de actividad
const validateActivity = async (req, res, activity) => {
    // validar existencia de la actividad
    const activityExist = await Activity.findById(activity);
    if (!activityExist) {
        return res.status(404).json({ msg: 'Activity dont exist' });
    }

    // validar dueño o miembro de actividad
    if (activityExist.owner.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'Unauthorized' });
    }
}

// crear tarea
exports.newTask = async (req, res) => {
    // revisar errores 
    issues(req, res);

    try {
        // extraer actividad del req.body
        const { activity } = req.body;

        // validar existencia y membresia de actividad
        validateActivity(req, res, activity);

        // crear tarea con base al modelo
        const task = new Task(req.body);

        // guardar tarea
        await task.save();
        res.json(task);
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}


// obtener tareas
exports.taskList = async (req, res) => {    
    try {
        // extraer actividad del req.query
        // se envia por params -> {params: {activity}} -> esto viene en la (url)
        const { activity } = req.query;

        // validar existencia y membresia de actividad
        validateActivity(req, res, activity);

        // obtener lista de tareas por actividad
        const tasks = await Task.find({activity: activity}).sort({timeStamp: -1});
        res.json(tasks);
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}


// editar tarea
exports.updateTask = async (req, res) => {
    // revisar errores 
    issues(req, res);

    try {
        // extraer elementos de req.body
        const { name, status, activity } = req.body;

        // verificar existencia de tarea
        let task = await Task.findById(req.params.id);
        if (!task) {
            return res.status(404).json({ msg: 'Activity dont exist' });
        }

        // validar existencia y membresia de actividad
        validateActivity(req, res, activity);

        // crear nuevo objeto que almacene los datos a editar
        const newTask = {};

        // de acuerdo a lo que cambie
        newTask.name = name != null ? name : task.name;
        newTask.status = status != null ? status : task.state;

        // guardar tarea
        task = await Task.findOneAndUpdate({_id:req.params.id}, newTask, {new: true});
        res.json(task);
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}


// eliminar tarea
exports.deleteTask = async (req, res) => {
    try {
        // extraer actividad de req.query
        const { activity } = req.query; /*  <- .query porque se envia por params desde cliente */

        // verificar existencia de tarea
        let task = await Task.findById(req.params.id);
        if (!task) {
            return res.status(404).json({ msg: 'Activity dont exist' });
        }

        // validar existencia y membresia de actividad
        validateActivity(req, res, activity);

        // eliminar tarea
        await Task.findByIdAndRemove({_id: req.params.id});
        res.json({msg: 'Task deleted'});
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}
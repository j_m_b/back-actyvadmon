const User = require('../models/User');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

// funcion que verifica errores definidos en las rutas
function issues(req, res){
    const errors = validationResult(req);
    if(!errors.isEmpty(0)){
        return res.status(400).json({errors: errors.array()});
    }
}

// autentica un usuario
exports.authUser = async (req, res) => {
    // revisar si hay errores
    issues(req, res);

    // extraer email y password
    const { email, password } = req.body;

    try {
        // validar usuario registrado
        let user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ msg: 'User not exist' });
        }

        // validar password de usuario
        const passwordCorrect = await bcryptjs.compare(password, user.password);
        if (!passwordCorrect) {
            return res.status(400).json({ msg: 'Password incorrect' });
        }

        // si pasa validaciones: crear y firmar el jwt (JsonWebToken)
        const payload = {
            user: {
                id: user.id
            }
        };
        // firmar jwt
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: 7200
        }, (error, token) => {
            if(error) throw error;

            // mensaje de confirmacion
            res.json({ token });
        });

    } catch (error) {
        console.log(error);
    }
}


// devuelve el objeto usuario al momento de autenticarse, pero sin la contraseña
exports.userAuthenticated = async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json({ user });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'An error happen' });
    }
}
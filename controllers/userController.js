const User = require('../models/User');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

// funcion que verifica errores definidos en las rutas
function issues(req, res){
    const errors = validationResult(req);
    if(!errors.isEmpty(0)){
        return res.status(400).json({errors: errors.array()});
    }
}

// crear un usuario nuevo
exports.newUser = async (req, res) => {
    // revisar si hay errores
    issues(req, res);

    // extraer email y password del request.body
    const { email, password } = req.body;

    try {
        // validar que usuario registrado sea unico por el email
        let user = await User.findOne({ email });
        if (user) {
            return res.status(409).json({ msg: 'User already exist' });
        }

        // si pasa validacion, crear usuario con model User
        user = new User(req.body);

        // hash con bcrypt al password
        const salt = await bcryptjs.genSalt(10);
        user.password = await bcryptjs.hash(password, salt);

        // guardar usuario
        await user.save();

        // crear y firmar el jwt (JsonWebToken)
        const payload = {
            user: {
                id: user.id
            }
        };
        // firmar el jwt
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: 7200
        }, (error, token) => {
            if(error) throw error;

            // mensaje de confirmacion
            res.json({ token })
        });

    } catch (error) {
        console.log(error);
        res.status(500).send('An error happen');
    }
}
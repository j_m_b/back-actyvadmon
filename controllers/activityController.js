const Activity = require('../models/Activity');
const User = require('../models/User');
const { validationResult } = require('express-validator');


// funcion que verifica errores definidos en las rutas
function issues(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty(0)) {
        return res.status(400).json({ errors: errors.array() });
    }
}


// crear actividad
exports.newActivity = async (req, res) => {
    // revisar si hay errores
    issues(req, res);

    try {
        // crear actividad con base al modelo
        const activity = new Activity(req.body);

        // guardar dueño de actividad
        activity.owner = req.user.id;

        // guardar actividad
        activity.save();
        res.json(activity);
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}


// obtener lista de actividades 
exports.activityList = async (req, res) => {
    try {
        // ! NO ENVIAR PASSWORD DE USUARIO EN COLECCION ¡
        const response = await Activity.find({ $or:[{owner: req.user.id}, {members: req.user.id}] })
            .sort({ timeStamp: -1 }).populate('members');;
        res.json(response);
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}

exports.activity = async (req, res) => {
    try {
        // verificar que usuario pueda acceder a actividad
        const response = await Activity.find({_id:req.params.id, $or:[{owner: req.user.id}, {members: req.user.id}] })
            .populate('members');
        res.json(response);
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}


// actualizar actividad
exports.updateActivity = async (req, res) => {
    // revisar errores
    issues(req, res);

    // extraer info del request body
    const { name, email, actionDelete } = req.body;

    try {
        // revisar si el proyecto existe
        let activity = await Activity.findById(req.params.id);
        if (!activity) {
            return res.status(404).json({ msg: 'The items does not exist' });
        }

        // validar dueño de actividad
        if (activity.owner.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'Unauthorized' });
        }


        // nuevo objevo para depositar los cambios
        const new_activity = {};
        if (name) {
            new_activity.name = name;
        }

        if (email) {
            // si usuario existe
            const user = await User.findOne({ email });
            console.log(user);
            if (!user) {
                return res.status(404).json({ msg: 'The items does not exist' });
            }

            // si usuario es dueño 
            if (user._id.toString() === req.user.id) {
                return res.status(409).json({ msg: 'You are a current member' });
            }

            // si usuario ya esta en actividad, verificando que lo que quiero hacer es agregar 
            if (activity.members.includes(user._id) && !actionDelete) {
                return res.status(409).json({ msg: 'The user is already in the activity member list' });
            }

            new_activity.members = activity.members
            if (!actionDelete) {
                // agrego usuario a actividad
                new_activity.members.push(user);
            } else {
                const index = new_activity.members.indexOf(user._id);
                if (index > -1) {
                    new_activity.members.splice(index, 1);
                }
            }
        }

        // actualizar
        activity = await Activity.findByIdAndUpdate({ _id: req.params.id }, { $set: new_activity }, { new: true }).populate('members');
        res.json({ activity });
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}


// eliminar actividad
exports.removeActivity = async (req, res) => {
    try {
        // revisar si el proyecto existe
        let activity = await Activity.findById(req.params.id);
        if (!activity) {
            return res.status(404).json({ msg: 'The items does not exist' });
        }

        // validar dueño de actividad
        if (activity.owner.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'Unauthorized' });
        }

        // eliminar actividad
        await Activity.findByIdAndRemove({ _id: req.params.id });
        res.json({ msg: 'Activity removed' });
    } catch (error) {
        console.log(error);
        res.status(500).send('There is an error');
    }
}
const mongoose = require('mongoose');

const ActivitySchema = mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
    },
    tasks : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'task'
    }],
    members:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user',
        }
    ],
    timeStamp: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('activity', ActivitySchema);
const mongoose = require('mongoose');

// modelo de usuario
const UserSchema = mongoose.Schema({
    userName: {
        type: String,
        require: true,
        trim: true
    },
    email: {
        type: String,
        require: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        require: true,
        trim: true
    },
    activities: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'activity'
    }],
    timeStamp: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('user', UserSchema);
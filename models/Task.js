const moongose = require('mongoose');

const TaskSchema = moongose.Schema({
    name: {
        type: String,
        requiere: true,
    },
    status: {
        type: Boolean,
        default: false,
    },
    activity: {
        type: moongose.Schema.Types.ObjectId,
        ref: 'activity',
    },
    timeStamp: {
        type: Date,
        default: Date.now()
    }
});

module.exports = moongose.model('task', TaskSchema);

const mongoose = require('mongoose');
require('dotenv').config({path: 'variables.env'}); /* <- lectura de las variables de entorno */

const DBConection = async () => {
    console.log("Conecting to DB ...");
    try {
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useCreateIndex: true,
            useUnifiedTopology: true,
            autoIndex: false,
            poolSize: 10,
            bufferMaxEntries: 0
        });
        console.log('Data base is running on')
    } catch (error) {
        console.log("Some errors:");
        console.log(error);
        process.exit(1);
    }
}

module.exports = DBConection;
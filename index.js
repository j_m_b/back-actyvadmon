const express = require('express');
const DBConection = require('./config/db');
const cors = require('cors'); // cross origin resource sharing, 

// crear el servidor
const app = express();

// conectar la base de datos
DBConection();

// habilitar cors para agregar encabesados http a las consultas en back
app.use(cors());

// habilitar express.json para leer datos que el usuario coloque
app.use(express.json({ extended: true }));

// puerto de la app
const PORT = process.env.PORT || 4000;

// // definir pagina principal
app.get('/', (req, res) => {
    res.send('hola mundo');
});

// importar rutas 
app.use('/api/user', require('./routes/user'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/activities', require('./routes/activities'));
app.use('/api/tasks', require('./routes/tasks'));


// arrancar la app
app.listen(PORT, () => {
    console.log(`Server port: ${PORT}`);
});
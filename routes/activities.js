const express = require('express');
const router = express.Router();
const activityController = require('../controllers/activityController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');


/* ->> /api/activities <<- */
// nueva actividad
router.post('/',
    auth,
    [
        check('name').not().isEmpty().withMessage('Name cannot be empty')
    ],
    activityController.newActivity
);

// obtener actividades
router.get('/',
    auth,
    activityController.activityList
);

// obtener una sola actividad
router.get('/:id',
    auth,
    activityController.activity
);

// actualizar actividad
router.put('/:id',
    auth,
    [
        check('name').not().isEmpty().withMessage('Name cannot be empty')
    ],
    activityController.updateActivity
);

// eliminar actividad
router.delete('/:id',
    auth,
    activityController.removeActivity
);

module.exports = router;
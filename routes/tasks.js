const express = require('express')
const router = express.Router();
const taskController = require('../controllers/taskController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');


// api/tasks
// crear tarea
router.post('/',
    auth,
    [
        check('name').not().isEmpty().withMessage('Name cannot be empty'),
        check('activity').not().isEmpty().withMessage('Activity cannot be null or empty'),
    ],
    taskController.newTask
);

// listar tareas
router.get('/',
    auth,
    taskController.taskList
);

// editar tarea
router.put('/:id',
    auth,
    taskController.updateTask
);

// eliminar tarea
router.delete('/:id',
    auth,
    taskController.deleteTask
);


module.exports = router;
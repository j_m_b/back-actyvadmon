// ruta para autenticar login de usuario

const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const authController = require('../controllers/authController');
const auth = require('../middleware/auth');


/* ->> /api/auth <<- */
// iniciar sesion
router.post('/',
    [
        check('email').isEmail().withMessage('Invalid email format'),
        check('password').isLength({ min: 6 }).withMessage('Password mush have min 6 characteres')
    ],
    authController.authUser
);

router.get('/',
    auth,
    authController.userAuthenticated
);

module.exports = router;
// rutas para crear usuario

const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const { check } = require('express-validator');


/* ->> /api/user <<- */
// Registrar usuario
router.post('/',
    [
        check('userName').not().isEmpty().withMessage('User name cannot be empty'),
        check('email').isEmail().withMessage('Invalid email format'),
        check('password').isLength({ min: 6 }).withMessage('Password mush have min 6 characteres')
    ],
    userController.newUser
);

module.exports = router;